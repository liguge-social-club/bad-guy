\version "2.18.2"

PART_NAME = "Voix 2"

\include "../main.ily"

music = \"staff voix 2"
midiMusic = \music

\include "../output/part.ily"
\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
