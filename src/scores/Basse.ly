\version "2.18.2"

PART_NAME = "Basse"

\include "../main.ily"

music = \"staff basse"
midiMusic = \music

\include "../output/part.ily"
\include "../output/transpo-si-b.ily"

music = \"staff basse sax bar"
\include "../output/transpo-mi-b.ily"
