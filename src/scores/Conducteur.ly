\version "2.18.2"

PART_NAME = "Conducteur"

\include "../main.ily"

music = <<
    \"staff voix 1"
    \"staff voix 2"
    \"staff voix 3"
    \"staff voix 4"
    \"staff basse"
>>

midiMusic = <<
  \"staff voix 1"
  \"staff voix 2"
  \"staff voix 3"
  \"staff voix 4"
  \"staff basse"
  \"staff drums"
>>

\include "../output/conducteur.ily"
\include "../output/transpo-si-b.ily"
