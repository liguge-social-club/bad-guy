\version "2.18.2"

PART_NAME = "Voix 4"

\include "../main.ily"

music = \"staff voix 4"
midiMusic = \music

\include "../output/part.ily"
\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
