\version "2.18.2"

PART_NAME = "Voix 1"

\include "../main.ily"

music = \"staff voix 1"
midiMusic = \music

\include "../output/part.ily"
\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
