\version "2.18.2"

PART_NAME = "Quatre voix mini"

\include "../main.ily"

music = <<
    \"staff voix 1-2"
    \"staff voix 3-4"
>>
midiMusic = \music

\include "../output/conducteur-no-midi.ily"
\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
