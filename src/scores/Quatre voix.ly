\version "2.18.2"

PART_NAME = "Quatre voix"

\include "../main.ily"

music = <<
    \"staff voix 1"
    \"staff voix 2"
    \"staff voix 3"
    \"staff voix 4"
>>
midiMusic = \music

\include "../output/conducteur.ily"
\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
