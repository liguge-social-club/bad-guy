% Définit une couleur de fond:
#(define-markup-command (on-color layout props color arg) (color? markup?)
   (let* ((stencil (interpret-markup layout props arg))
          (X-ext (ly:stencil-extent stencil X))
          (Y-ext (ly:stencil-extent stencil Y)))
     (ly:stencil-add (ly:make-stencil
                      (list 'color color
                        (ly:stencil-expr (ly:round-filled-box X-ext Y-ext 0))
                        X-ext Y-ext)) stencil)))


% Écrit "DA." sur la partition:
"DA." = {
  \once \override Rest.stencil = #(lambda (grob)
    (grob-interpret-markup grob #{
      \markup {
        \fontsize #6
        \bold
        \rotate #45.0
        % \lower #1
        \halign #-0.4
        \typewriter
        \on-color #white
        "DA."
      }
    #})
  )
  r4
}
