my_mark = #(define-music-function (text) (string?) #{
  \mark \markup {
    \override #'(box-padding . 0.5)
    \box
    \bold
    #text
  }
#})

"mark - Couplet A" = \my_mark "Couplet A"
"mark - Refrain A" = \my_mark "Refrain A"
"mark - Gimmick A" = \my_mark "Gimmick A"
"mark - Couplet B" = \my_mark "Couplet B"
"mark - Refrain B" = \my_mark "Refrain B"
"mark - Gimmick B" = \my_mark "Gimmick B"
