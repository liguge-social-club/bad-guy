% Dynamiques MIDI:
dyn_full_range = #(cons 0 1)
dyn_loud = #(cons 0.2 0.5)
dyn_soft = #(cons 0.1 0.2)

midiVolume = #(define-music-function (vol) (pair?) #{
  \set midiMaximumVolume = #(cdr vol)
  \set midiMinimumVolume = #(car vol)
#})
