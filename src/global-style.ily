#(set-global-staff-size 16)

#(define (not-part-first-page layout props arg)
  (if (not (part-first-page? layout props))
    (interpret-markup layout props arg)
    empty-stencil))
#(define (print-page-number-check-first layout props arg)
  (if (or (not (book-first-page? layout props))
    (eq? (ly:output-def-lookup layout 'print-first-page-number) #t))
    (create-page-number-stencil layout props arg)
    empty-stencil))

\header {
  title = \SONG_NAME
  instrument = \PART_NAME
  tagline = ##f
  copyright = "Ligugé Social Club - 2021"
}

HEADER_MIDDLE = \markup \concat {
  \fromproperty #'header:title
  " - "
  \fromproperty #'header:instrument
}

\paper {
  oddHeaderMarkup = \markup
  \fill-line {
    ""
    \on-the-fly #not-part-first-page \HEADER_MIDDLE
    \on-the-fly #print-page-number-check-first \fromproperty #'page:page-number-string
  }
  evenHeaderMarkup = \markup
  \fill-line {
    \on-the-fly #print-page-number-check-first \fromproperty #'page:page-number-string
    \on-the-fly #not-part-first-page \HEADER_MIDDLE
    ""
  }
}

\layout {
  \override MultiMeasureRest.expand-limit = #3
  \context {
    \Score
    \override SpacingSpanner.spacing-increment = #3.0
    \override MetronomeMark.Y-offset = #'6
  }
}
