"couplet simple - voix 3" = \absolute {
  % Première partie du couplet:

  % Bar 9
  bf4\staccato bf\staccato r8 bf r bf |

  % Bar 10
  r8 bf r  bf r bf c'4 |

  % Bar 11
  bf4\staccato bf\staccato r8 bf r bf |

  % Bar 12
  r8 bf r  bf r bf c'4 |

  % Bar 13
  bf4\staccato bf\staccato r8 bf r bf |

  % Bar 14
  r8 bf r bf r bf( d'4) |

  % Bar 15
  c'4\staccato  c'\staccato r8 c' r c' |

  % Bar 16
  r8 bf  a   r r2 |
}

"couplet tutti - voix 3" = \absolute {
  % Bar 17
  bf4\staccato bf\staccato r8 bf r bf |

  % Bar 18
  r8 bf r bf r bf c'4 |

  % Bar 19
  bf4\staccato bf\staccato r8 bf r bf |

  % Bar 20
  r8 bf r bf r bf c'4 |

  % Bar 21
  bf4\staccato  bf\staccato  r8 bf  r bf |

  % Bar 22
  r8 bf  r bf  r bf(  d'4) |

  % Bar 23
  c'4\staccato  c'\staccato  r8 c'  r c' |

  % Bar 24
  r8 ef' d' r  r2 |
}

"refrain - voix 3" = {
  % Bar 25-27
  \repeat unfold 3 {
    d'4 bf bf8( d') d'( c') |
  }

  % Bar 28
  d'4 bf r2 |

  % Bar 29-30
  \repeat unfold 2 {
    c'4  g  g8  g  g  g |
  }

  % Bar 31
  d'4  a  a8  a  a  a |

  % Bar 32
  d'4  a r c'8 bf |
}

"I'm a bad guy - voix 3" = {
  % Bar 33
  a2~  8 g4.~ |

  % Bar 34
  g4  r4 r \"DA." |
}

"gimmick - voix 3" = {
  \tag #'noExtra { R1*7 }

  \tag #'withExtra {
    % Bar 35-38
    \repeat unfold 4 {
      r4. bf8 a bf a bf |
    }

    % Bar 39-40
    \repeat unfold 2 {
      r4. bf8 a bf a bf |
    }

    % Bar 41
    r4. bf8 a bf a bf |
  }

  % Bar 42
  \tag #'noDa   { R1         }
  \tag #'withDa { r2. \"DA." } |
}

"thème voix 3" = {
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % INTRO
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  R1*8

  \absolute {
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % COUPLET
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 9
    \"mark - Couplet A"
    \"couplet simple - voix 3"
    % Bar 17
    \"couplet tutti - voix 3"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 25-32
    \"mark - Refrain A"
    \"refrain - voix 3"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY...
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 33-34
    \"I'm a bad guy - voix 3"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 35-42
    \"mark - Gimmick A"
    R1*8

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A CAPELLA / COUPLET B  ==> 1:13
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 43
    \"mark - Couplet B"
    \"couplet tutti - voix 3"

    % Bar 51
    \"couplet tutti - voix 3"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 59
    \"mark - Refrain B"
    \"refrain - voix 3"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY... B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 67-68
    \"I'm a bad guy - voix 3"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK B  ==> 2:00
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 69-84
    \"mark - Gimmick B"
    \keepWithTag #'(#'noExtra #'withDa) \"gimmick - voix 3"
    \keepWithTag #'(#'withExtra #'noDa) \"gimmick - voix 3"  % ==> 2:15
  }

  \bar ":|."
}
