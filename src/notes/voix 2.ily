"couplet simple - voix 2" = \absolute {
  % Première partie du couplet:

  % Bar 9-12
  R1*4

  % Bar 13
  d'4\staccato d'\staccato r8 d' r d' |

  % Bar 14
  r8 d' r d' r d'( f'4) |

  % Bar 15
  d'4\staccato d'\staccato r8 d' r d' |

  % Bar 16
  r8 ef' d' r r2 |
}

"couplet tutti - voix 2" = \absolute {
  % Bar 17
  d'4\staccato d'\staccato r8 d' r d' |

  % Bar 18
  r8 d' r d' r d' ef'4 |

  % Bar 19
  d'4\staccato d'\staccato r8 d' r d' |

  % Bar 20
  r8 d' r d' r d' ef'4 |

  % Bar 21
  ef'4\staccato  ef'\staccato  r8 ef'  r ef' |

  % Bar 22
  r8 ef'  r ef'  r ef'(  d'4) |

  % Bar 23
  fs'4\staccato fs'\staccato r8 fs' r fs' |

  % Bar 24
  r8 bf'  a'  r  r2 |
}

"refrain - voix 2" = {
  % Bar 25-26
  R1*4

  % Bar 29-30
  \repeat unfold 2 {
    bf'4 f' r2 |
  }

  % Bar 31
  a'4 fs' d'8  d'  d'  d' |

  % Bar 32
  c'4  d' r c''8 bf' |
}

"I'm a bad guy - voix 2" = {
  % Bar 33
  a'2~ 8 g'4.~ |

  % Bar 34
  g'4 r4 r \"DA." |
}

"gimmick - voix 2" = {
  \tag #'noExtra { R1*7 }

  \tag #'withExtra {
      % Bar 35-38
      \repeat unfold 4 {
        r4. d'8 cs' d' cs' d' |
      }

      % Bar 39-40
      \repeat unfold 2 {
        r4. d'8 cs' d' cs' d' |
      }

      % Bar 41
      r4. d'8 cs' d' cs' d' |
  }

  % Bar 42
  \tag #'noDa   { R1         }
  \tag #'withDa { r2. \"DA." } |
}

"thème voix 2" = {
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % INTRO
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  R1*8

  \absolute {
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % COUPLET
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 9
    \"mark - Couplet A"
    \"couplet simple - voix 2"
    % Bar 17
    \"couplet tutti - voix 2"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 25-32
    \"mark - Refrain A"
    \"refrain - voix 2"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY...
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 33-34
    \"I'm a bad guy - voix 2"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 35-42
    \"mark - Gimmick A"
    R1*8

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A CAPELLA / COUPLET B  ==> 1:13
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 43
    \"mark - Couplet B"
    \"couplet tutti - voix 2"

    % Bar 51
    \"couplet tutti - voix 2"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 59
    \"mark - Refrain B"
    \"refrain - voix 2"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY... B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 67-68
    \"I'm a bad guy - voix 2"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK B  ==> 2:00
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 69-84
    \"mark - Gimmick B"
    \keepWithTag #'(#'noExtra #'withDa) \"gimmick - voix 2"
    \keepWithTag #'(#'withExtra #'noDa) \"gimmick - voix 2"  % ==> 2:15
  }

  \bar ":|."
}
