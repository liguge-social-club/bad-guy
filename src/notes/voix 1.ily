"couplet simple - voix 1" = \absolute {
  % Première partie du couplet:
  % Bar 9-16
  R1*8
}

debutCoupletTuttiVoixUn = \absolute {
  % Bar 17-20
  R1*4

  % Bar 21
  bf'4\staccato  bf'\staccato  r8 bf'  r bf' |

  % Bar 22
  r8 bf'  r bf'  r bf'(  d''4) |

  % Bar 23
  a'4\staccato a'\staccato r8 a' r a' |
}

"couplet tutti - voix 1" = #(define-music-function
     (to_refrain)
     (boolean?)
#{
  \absolute {
    % Deuxième partie du couplet:

    \debutCoupletTuttiVoixUn

    % Bar 24
    #( if to_refrain
      #{ r8 g' fs' r r8  bf' bf'( c'') | #}
      #{ r8 g' fs' r r2                | #}
    )
  }
#})

"refrain - voix 1" = {
  % Bar 25-27
  \repeat unfold 3 {
    c''4 g'  bf'8( c'') c''( bf') |
  }

  % Bar 28
  c''4 g'  r4 bf'8 c'' |

  % Bar 29-30
  \repeat unfold 2 {
    f''4  d'' c''8 c'' c'' bf' |
  }

  % Bar 31
  f''4 d''  c''8 c'' c'' bf' |

  % Bar 32
  f''4 d'' r2 |
}

"I'm a bad guy - voix 1" = {
  % Bar 33
  R1 |

  % Bar 34
  r2.      \"DA." |
}

"gimmick - voix 1" = {
  % Bar 35-38
  \repeat unfold 4 {
    g'8( d'')   r g'  cs''(  d'')  cs''(  bf') |
  }

  % Bar 39-40
  \repeat unfold 2 {
    c''8(  g'')  r c''  fs''(  g'')  fs''(  ef'') |
  }

  % Bar 41
  d''8(  a'')  r d''  fs''(  d'')  r c''~ |

  % Bar 42
  \tag #'noDa   { c''2  r2        }
  \tag #'withDa { c''2  r4 \"DA." } |
}

"thème voix 1" = {
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % INTRO
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  R1*8

  \absolute {
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % COUPLET
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 9
    \"mark - Couplet A"
    \"couplet simple - voix 1"
    % Bar 17
    \"couplet tutti - voix 1" ##t

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 25-32
    \"mark - Refrain A"
    \"refrain - voix 1"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY...
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 33-34
    \"I'm a bad guy - voix 1"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 35-42
    \"mark - Gimmick A"
    \keepWithTag #'(#'noExtra #'noDa) \"gimmick - voix 1"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A CAPELLA / COUPLET B  ==> 1:13
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 43
    \"mark - Couplet B"
    \"couplet tutti - voix 1" ##f

    % Bar 51
    \"couplet tutti - voix 1" ##t

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 59
    \"mark - Refrain B"
    \"refrain - voix 1"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY... B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 67-68
    \"I'm a bad guy - voix 1"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK B  ==> 2:00
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 69-84
    \"mark - Gimmick B"
    \keepWithTag #'(#'noExtra #'withDa) \"gimmick - voix 1"
    \keepWithTag #'(#'withExtra #'noDa) \"gimmick - voix 1"  % ==> 2:15
  }

  \bar ":|."
}
