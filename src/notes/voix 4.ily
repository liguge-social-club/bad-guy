"couplet simple - voix 4" = \absolute {
  % Première partie du couplet:

  % Bar 9
  R1 |

  % Bar 10
  r4    r8 g  r g  a4 |

  % Bar 11
  R1 |

  % Bar 12
  r4    r8 g  r g  a4 |

  % Bar 13
  g4\staccato  g\staccato  r8 g  r g |

  % Bar 14
  r8 g  r g  r g(  bf4) |

  % Bar 15
  fs4\staccato   fs\staccato  r8 fs  r fs |

  % Bar 16
  r8 ef  d   r r2 |
}

"couplet tutti - voix 4" = \absolute {
  % Bar 17
  g4\staccato  g\staccato  r8 g  r g |

  % Bar 18
  r8 g  r g  r g  a4 |

  % Bar 19
  g4\staccato  g\staccato  r8 g  r g |

  % Bar 20
  r8 g  r g  r g  a4 |

  % Bar 21
  g4\staccato   g\staccato   r8 g   r g |

  % Bar 22
  r8 g   r g   r g(   bf4) |

  % Bar 23
  a4\staccato   a\staccato   r8 a   r fs |

  % Bar 24
  r8 g   fs r r2 |
}

"refrain - voix 4" = {
  % Bar 25-27
  \repeat unfold 3 {
    g4  r  g8(  g)  g(  c') |
  }

  % Bar 28
  bf4 g  r2 |

  % Bar 29-32
  R1*4
}

"I'm a bad guy - voix 4" = {
  % Bar 33
  R1 |

  % Bar 34
  r2. \"DA." |
}

"gimmick - voix 4" = {
  % Bar 35-38
  \repeat unfold 4 {
    g8( d')   r g  cs'(  d')  cs'(  bf) |
  }

  % Bar 39-40
  \repeat unfold 2 {
    c8(  g)  r c  fs(  g)  fs(  ef) |
  }

  % Bar 41
  d8(  a)  r d  fs(  d)  r c~ |

  % Bar 42
  \tag #'noDa   { c2  r2        }
  \tag #'withDa { c2  r4 \"DA." } |
}

"thème voix 4" = {
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % INTRO
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  R1*8

  \absolute {
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % COUPLET
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 9
    \"mark - Couplet A"
    \"couplet simple - voix 4"
    % Bar 17
    \"couplet tutti - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 25-32
    \"mark - Refrain A"
    \"refrain - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY...
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 33-34
    \"I'm a bad guy - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 35-42
    \"mark - Gimmick A"
    \keepWithTag #'(#'noExtra #'noDa) \"gimmick - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A CAPELLA / COUPLET B  ==> 1:13
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 43
    \"mark - Couplet B"
    \"couplet tutti - voix 4"

    % Bar 51
    \"couplet tutti - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % REFRAIN B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 59
    \"mark - Refrain B"
    \"refrain - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % I'M A BAD GUY... B
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 67-68
    \"I'm a bad guy - voix 4"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % GIMMICK B  ==> 2:00
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Bar 69-84
    \"mark - Gimmick B"
    \keepWithTag #'(#'noExtra #'withDa) \"gimmick - voix 4"
    \keepWithTag #'(#'withExtra #'noDa) \"gimmick - voix 4"  % ==> 2:15
  }

  \bar ":|."
}
