staffCommon = {
  \key g \minor
  \time 4/4
  \tempo 4 = \TEMPO
}

staff = #(define-music-function
  (name    clef    music     vol   midiName)
  (string? string? ly:music? pair? string?)
  #{
    \new Staff \with {
      instrumentName = #name
      midiInstrument = #midiName
    } {
      \clef #clef
      \staffCommon
      \midiVolume #vol
      \compressMMRests
      #music
    }
  #})

staffSaxBar = #(define-music-function
  (name    music)
  (string? ly:music?)
  #{
    \staff #name "treble" { \transpose c c''' #music } \dyn_full_range "baritone sax"
  #})

%----------------------  NOM-----  CLEF----  MUSIQUE--------  VOL. MIDI------  INSTR. MIDI-------------
"staff voix 1" = \staff  "Voix 1"  treble    \"thème voix 1"  \dyn_loud        "alto sax"
"staff voix 2" = \staff  "Voix 2"  treble    \"thème voix 2"  \dyn_loud        "alto sax"
"staff voix 3" = \staff  "Voix 3"  bass      \"thème voix 3"  \dyn_loud        "alto sax"
"staff voix 4" = \staff  "Voix 4"  bass      \"thème voix 4"  \dyn_loud        "alto sax"
"staff basse"  = \staff  "Basse"   "bass_8"  \part_bass       \dyn_full_range  "electric bass (finger)"
"staff basse sax bar" = \staffSaxBar "Basse" \part_bass
"staff drums" = <<
  \new DrumStaff {
    \part_drums
  }
  \new DrumStaff {
    \midiVolume \dyn_loud
    \hand_claps
  }
>>

"thème voix 1-2" = \new Voice <<
   \"thème voix 1"
   \\
   \"thème voix 2"
>>
"thème voix 3-4" = \new Voice <<
   \"thème voix 3"
   \\
   \"thème voix 4"
>>
"staff voix 1-2" = \staff  "Voix 1-2"  treble    \"thème voix 1-2"  \dyn_loud        "alto sax"
"staff voix 3-4" = \staff  "Voix 3-4"  bass      \"thème voix 3-4"  \dyn_loud        "alto sax"
