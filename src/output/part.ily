\paper {
  markup-system-spacing = #'(
    (basic-distance . 18)
    (minimum-distance . 6)
    (padding . 1)
    (stretchability . 12)
  )
  system-system-spacing = #'(
    (basic-distance . 11)
    (minimum-distance . 6)
    (padding . 1)
    (stretchability . 12)
  )
}

\book {
  \bookOutputName #(format "~a - ~a" SONG_NAME PART_NAME)
  \score {
    \new StaffGroup \music
    \layout {}
  }

  \score {
    \articulate \midiMusic
    \midi {}
  }
}
