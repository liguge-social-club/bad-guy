\book {
  \bookOutputName #(format "~a - ~a (Transpo Si-b)" SONG_NAME PART_NAME)
  \header {
      instrument = #(format "~a - Transpo en Si♭" PART_NAME)
  }
  \score {
    \new StaffGroup \transpose bf c' \music
    \layout {}
  }
}
