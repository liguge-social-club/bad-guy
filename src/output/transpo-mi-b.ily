\book {
  \bookOutputName #(format "~a - ~a (Transpo Mi-b)" SONG_NAME PART_NAME)
  \header {
      instrument = #(format "~a - Transpo en Mi♭" PART_NAME)
  }
  \score {
    \new StaffGroup \transpose ef c \music
    \layout {}
  }
}
