\version "2.18.2"
\language "english"

\include "articulate.ly"

\include "./include/env_util.ily"
#(begin
  (define TEMPO (number-from-env "TEMPO" 135))
  (define SONG_NAME (string-from-env "SONG_NAME" "No name"))
)


\include "./include/da.ily"
\include "./include/midi.ily"
\include "./include/marks.ily"
\include "./notes/all.ily"
\include "./staves.ily"
\include "./global-style.ily"
