bash-project-boilerplate
========================

Skeleton to create complex bash commands, with eased bash completion per subcommand.

The project is designed to handle git-like command-based cli arguments, with the following general
form:

    my_command [GLOBAL ARGS...] <COMMAND> [COMMAND ARGS...]

Initial configuration / installation
====================================

After having cloned the project, choose a name for your project, let's say `fuzzyfoo`.

Rename the main root bash script:

    $ mv project-boilerplate fuzzyfoo

In the main bash script, change the value of the variable `$project_name`:

    project_name=fuzzyfoo

Install the script in a `$PATH` directory, for example by linking it:

    # ln -s $(realpath fuzzyfoo) /usr/local/bin/fuzzyfoo

### Setting up bash completion

*Prerequisite*: The `bash-completion` project must be installed on the target system.

In order to make bash completion work, edit the file `src/complete.sh` and change the value of the
variable `$PROJECT_BOILERPLATE_PROJECT_NAME` to the project name:

    PROJECT_BOILERPLATE_PROJECT_NAME=fuzzyfoo

Then create a link to this file into `/usr/share/bash-completion/completions`:

    # ln -s $(realpath src/complete.sh) /usr/share/bash-completion/completions/fuzzyfoo

Creating a new command
======================

Let's say we want to setup a new subcommand named `mycommand`:

Create a file `src/commands/mycommand.sh`. The filename must be the exact name of the command with
`.sh` suffix. This will be detected as a new command by the project (autocompletion, help, running
the command).

Once this file will be setup as described below, the command will be available for use.

In the file, as a start, copy the content of the pre-existing help command `src/commands/help.sh`,
and adapt its content. All the functions present in the help command must be present:

### command\_help()

This function will be called when invoking `fuzzyfoo help mycommand`.

### command\_execute()

This is the main function: it will be called when invoking `fuzzyfoo mycommand ARG...`. The
arguments following the command are passed to this function as `$1`, `$2`....

### command\_complete()

This function will be called when trying to complete the command with bash. If no completion is
wished, just create an empty function:

    command_complete()
    {
        :
    }

For an simple completion, echo the wanted words / options:

    command_complete()
    {
        echo "--myoption1 --myoption2 myarg1 myarg2"
    }

For more complex completion, you can use the given arguments passed to the function:

* `$1` is the current word under the cursor
* `$2` is the previous word before the cursor
* `$3` is numerical position of the current word undeer the cursor
* Remaining arguments are the list of all arguments before the cursor

env file
========

A file named `env` can be added at the root of the project, it will be sourced by the script on
startup. This file will be ignored by version control.
