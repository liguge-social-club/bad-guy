#-------------------- command_help()
command_help()
{
    echo "build <WHAT> [SCORE]  Builds some output file."
}

#-------------------- build_score()
build_score()
{
    local score="$1"
    local export_dir="$PROJECT__EXPORTS_DIR/$score"

    is_score_name "$score" || fatal "'$score' is not a score name"

    mkdir -p "$export_dir"
    local tmp_dir=$(mktemp -p $PROJECT__EXPORTS_DIR -d)
    local lilypond_success=

    (
        cd "$tmp_dir"
        export SONG_NAME
        lilypond "$PROJECT__SCORES_DIR/$score.ly"
    ) && lilypond_success=1

    if [[ -n $lilypond_success ]]; then
        mv "$tmp_dir"/* "$export_dir"

        if [[ $GENERATE_MP3 == 1 ]]; then
            (
                set -eu
                local score_basename="$SONG_NAME - $score"
                local wav_file="$tmp_dir/$score.wav"
                local mp3_file="$export_dir/$score_basename.mp3"
                local midi_file="$export_dir/$score_basename.midi"
                if [[ -f $midi_file ]]; then
                    timidity "$midi_file" -Ow -o "$wav_file"
                    lame "$wav_file" "$mp3_file"
                    rm "$wav_file" 2>/dev/null
                fi
            )
        fi
    fi

    rm -r "$tmp_dir"
}

#-------------------- command_exec()
command_exec()
{
    local subsub_command="$1"
    shift
    export GENERATE_PDF_MIDI=0 GENERATE_MP3=0

    case $subsub_command in
        pdf-midi)
            GENERATE_PDF_MIDI=1
            ;;
        mp3)
            GENERATE_MP3=1
            ;;
        all|"")
            GENERATE_PDF_MIDI=1
            GENERATE_MP3=1
            ;;
        *)
            fatal "Command build: Unrecognized sub-command: ${bold}${subsub_command}${reset}"
    esac

    local score_names score_name
    if [[ -n $1 ]]; then
        score_names=( "$1" )
    else
        readarray -t score_names < <(get_score_names)
    fi
    for score_name in "${score_names[@]}"; do
        build_score "$score_name"
    done
}

#-------------------- command_complete()
command_complete()
{
    get_score_names
}
