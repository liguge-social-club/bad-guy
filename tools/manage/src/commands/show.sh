#-------------------- command_help()
command_help()
{
    echo "show scores  Prints the list of all score names"
}

#-------------------- command_exec()
command_exec()
{
	case $1 in
		scores|'') get_score_names ;;
		*) fatal "Unrecognized sub-command: $1"
	esac
}

#-------------------- command_complete()
command_complete()
{
	echo "scores"
}
