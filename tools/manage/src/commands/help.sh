#-------------------- command_help()
command_help()
{
    echo "help            Show global usage"
    echo "help <COMMAND>  Show detailed help for the given sub-command"
}

#-------------------- command_exec()
command_exec()
{
    if [[ -z $1 ]]; then
        help
    else
        local command_file="$SCRIPT_ROOT_DIR/src/commands/$1.sh"
        [[ ! -f  "$command_file" ]] && fatal "Unrecognized command: $cli_command"
        source "$command_file"
        command_help "${command_args[@]}"
    fi
}

#-------------------- command_complete()
command_complete()
{
    get_commands
}
