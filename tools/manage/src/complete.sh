
# Set your project name here (must match the binary name):
PROJECT_BOILERPLATE_PROJECT_NAME=manage

eval '
_complete_'$PROJECT_BOILERPLATE_PROJECT_NAME'()
{
    local cur prev words cword
    _init_completion || return

	local completions=$('$PROJECT_BOILERPLATE_PROJECT_NAME' --get-completion "$cur" "$prev" "$cword" "${words[@]}")
	COMPREPLY=( $(compgen -W "$completions" -- "$cur") )
} &&
complete -F _complete_'$PROJECT_BOILERPLATE_PROJECT_NAME' '$PROJECT_BOILERPLATE_PROJECT_NAME'
'

unset PROJECT_BOILERPLATE_PROJECT_NAME

