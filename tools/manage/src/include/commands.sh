
#-------------------- get_commands()
# Output a list of all available commands
get_commands()
{
	(
		cd "$SCRIPT_ROOT_DIR/src/commands"
		find -type f
	) | sed -e 's/\.sh//' -e 's/[./]\+//g'
}

#-------------------- get_command_file()
get_command_file()
{
    echo "$SCRIPT_ROOT_DIR/src/commands/$1.sh"
}

#-------------------- is_command()
is_command()
{
    [[ -f $(get_command_file "$1") ]]
}

#-------------------- must_be_a_command()
must_be_a_command()
{
    is_command "$1" || fatal "Unrecognized command: ${bold}$1${reset}"
}

#-------------------- source_command()
source_command()
{
    must_be_a_command "$1"
    local command_file=$(get_command_file "$1")
    source "$command_file"
}

#-------------------- execute_command()
execute_command()
{
    source_command "$1"
    shift
    command_exec "$@"
}

