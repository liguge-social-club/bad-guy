#-------------------- get_score_names()
get_score_names() {
	local score_name

	for lilypond_file in "$PROJECT__SCORES_DIR"/*.ly; do
		score_name="$(basename "$lilypond_file")"
		score_name="${score_name%%.ly}"
		echo "$score_name"
	done
}

#-------------------- is_score_name()
is_score_name() {
	! [[ $1 =~ [.~/] ]] && [[ -f "$PROJECT__SCORES_DIR/$1.ly" ]]
}
