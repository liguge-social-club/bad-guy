.PHONY: all clean pdf_midi watch_pdf_midi mp3

NAME = Bad guy
DIRS_TO_WATCH = src

all:
	@ ./manage build all

clean:
	@ rm ./exports -r

pdf_midi:
	@ ./manage build pdf-midi

pdf_midi_conducteur:
	@ ./manage build pdf-midi "Conducteur"

watch_pdf_midi:
	@ find $(DIRS_TO_WATCH) | entr sh -c '                                      \
		make pdf_midi;                                                          \
		echo;                                                                   \
		echo "--------------------------------------------------------------";  \
		echo;                                                                   \
	'

watch_pdf_midi_conducteur:
	@ find $(DIRS_TO_WATCH) | entr sh -c '                                      \
		make pdf_midi_conducteur;                                               \
		echo;                                                                   \
		echo "--------------------------------------------------------------";  \
		echo;                                                                   \
	'
